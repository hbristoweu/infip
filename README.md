# infip
Simple Linux program that prints the IPv4 address of a given interface name

Nicer and safer than $ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'
