/* H. Bristow, 2015/11/13 
 * 
 * infip <interface_name>
 * 
 * Parameters
 * 	<Optional> String Interface name
 * 
 * Returns
 * 	Exit success; Prints IP of given or first-available interface
 * 
 * Prints the IPv4 address of the given interface name in the first and
 * only parameter. When the paramter is not present, the first interface
 * found is used instead (It is usually the loopback address which is
 * always present in networking-compatible unix installations).
 * 
 * -std=c89 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h> 
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <ifaddrs.h>
