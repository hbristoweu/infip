/* H. Bristow, 2015/11/13
 *
 * infip <interface_name>
 *
 * Parameters
 * 	<Optional> String Interface name
 *
 * Returns
 * 	Exit success; Prints IP of given or first-available interface
 *
 * Prints the IPv4 address of the given interface name in the first and
 * only parameter. When the paramter is not present, the first interface
 * found is used instead (It is usually the loopback address which is
 * always present in networking-compatible unix installations).
 *
 * -std=c89 */

/* Todo:
 *  - Fix duplicate code in testInterfaceExists() and
 * 		getFirstInterface() - That 'if (getifaddrs(&addr))' check
 */

#include "infip.h"

void posix_error(int code, char *msg){

	fprintf(stderr,"%s: %s\n", strerror(code), msg);
	exit(EXIT_FAILURE);
}

int testInterfaceExists(char* s_ethname){

	struct ifaddrs *addr,*tmp;
	int ret = 0;

	/* Loopback will always exist on a TCP/IP networked machine */
	if (getifaddrs(&addr)){
		posix_error(ENONET, "No interfaces present on this system");

	}

	tmp = addr;
	while (tmp){
		/* strcmp: <1 Precedes, 0 Equal, >1 Follows */
		if (strcmp(s_ethname, tmp->ifa_name)==0){
			ret = 1;
			break;

		}

		tmp = tmp->ifa_next;

	}

	freeifaddrs(addr);

	return(ret);
}

char* getFirstInterface( void ){

	struct ifaddrs *addr,*tmp;
	char* ret = malloc(sizeof(char) * IFNAMSIZ);

	/* Loopback will always exist on a TCP/IP networked machine */
	if (getifaddrs(&addr)){
		posix_error(EINVAL, "No interfaces present on this system");

	}

	tmp = addr;
	ret = tmp->ifa_name;

	freeifaddrs(addr);

	return(ret);
}

char* getIP(char* s_ethname){

	int sockfd;
	struct ifreq intifr;
	char* ret = malloc(sizeof(char) * IFNAMSIZ);

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	intifr.ifr_addr.sa_family = AF_INET;

	strncpy(intifr.ifr_name, s_ethname, IFNAMSIZ-1);

	ioctl(sockfd, SIOCGIFADDR, &intifr);

	close(sockfd);

	ret = inet_ntoa(((struct sockaddr_in *)&intifr.ifr_addr)->sin_addr);

	return(ret);
}

int main(int argc, char* argv[]){

	char* ethname = malloc(sizeof(char) * IFNAMSIZ);

	if (argc == 1){
		ethname = getFirstInterface();

	} else if (argc == 2) {
		if (strlen(argv[1]) < IFNAMSIZ){
			if (testInterfaceExists(argv[1]))
				ethname = argv[1];

			else {
				posix_error(EINVAL, "Interface does not exist on this system");

			}

		} else {
			posix_error(EINVAL, "Interface name string too long");

		}

	} else {
		posix_error(E2BIG, "Too many arguments");

	}

	fprintf(stdout, "%s\n" ,getIP(ethname));

	exit(EXIT_SUCCESS);
}
